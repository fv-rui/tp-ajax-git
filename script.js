
/********* declaration des elements  */
/*** pour recuperer les valeurs à convertir */
let elConvertirSomme = document.getElementById("convertirSomme");
let elConvertirDevise = document.getElementById("convertirDevise");

/***** pour afficher le résultat */
let elAffResConversion = document.getElementById("affResConversion");
let elDeviseConverti = document.getElementById("deviseConverti");


let myHeaders = new Headers();
myHeaders.append("apikey", apiKey);

let requestOptions = {
  method: 'GET',
  redirect: 'follow',
  headers: myHeaders
};


async function getConversion(somme,deviseFrom,deviseTo){
    const response = await fetch(`https://api.apilayer.com/fixer/convert?to=${deviseTo}&from=${deviseFrom}&amount=${somme}`, requestOptions);
    const data = await response.json();

    console.log(data);
    
    return data;
}

async function deviseConversion(){

    let somme = elConvertirSomme.value;
    let deviseFrom = elConvertirDevise.value;

    let deviseTo = elDeviseConverti.value;

    if(deviseFrom !== deviseTo){
        let data = await getConversion(somme,deviseFrom,deviseTo);
        elAffResConversion.value = data.result;
    } else {
        alert("Les deux Devises sont identiques.");
    }
}